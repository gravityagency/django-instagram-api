from django.dispatch import Signal

in_api_call_signal = Signal(providing_args=["datetime"])